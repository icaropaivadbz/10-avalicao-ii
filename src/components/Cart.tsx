import React, { FunctionComponent } from 'react'
import './Cart.css'

interface IAppProps {
  name: string
  price: string
  quantity: number
  decrease: (param: string) => void
  increase: (param: string) => void
  remove: (param: string) => void
}

const Cart: FunctionComponent<IAppProps> = props => {
  return (
    <div className="containerProduct">
      <div className="product">
        <div className="product-info">
          <p>
            Nome: <br />
            <span>{props.name}</span>
          </p>
          <p>
            Preço: <br />
            <span>R$ {props.price}</span>
          </p>
          <div className="quantity">
            <div>
              Quantidade: <br />
              <div
                onClick={() => {
                  props.decrease(props.name)
                }}
              >
                <span>-</span>
              </div>
              <span>{props.quantity}</span>
              <div
                onClick={() => {
                  props.increase(props.name)
                }}
              >
                <span>+</span>
              </div>
            </div>
          </div>
        </div>
        <button
          onClick={() => {
            props.remove(props.name)
          }}
        >
          &#x274C;
        </button>
      </div>
    </div>
  )
}

export default Cart
