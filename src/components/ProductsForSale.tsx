import React, { FunctionComponent } from 'react'
import './ProductsForSale.css'

interface IAppProps {
  name: string
  price: number
  add: (name: string) => void
  unsubscribe: (name: string) => void
}

const ProductsForSale: FunctionComponent<IAppProps> = props => {
  return (
    <div className="banner">
      <p>
        <span>Nome do produto:</span> {props.name}
      </p>
      <p>
        <span>Preço do produto:</span> {`R$:${props.price}`}
      </p>

      <button onClick={() => props.add(props.name)}>
        &#x1F6D2; Adicionar &#x1F6D2;
      </button>

      <div className="button_remove">
        <button onClick={() => props.unsubscribe(props.name)}>&#x274C;</button>
      </div>
    </div>
  )
}

export default ProductsForSale
